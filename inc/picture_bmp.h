#pragma once
#include "picture_base.h"
#include "type_base.h"
#include <fstream>

typedef struct
{
	//unsigned short	bfType;		        // 2B 文件类型,BM,BA,CT,CP,IC,PT
	unsigned long	bfSize;				// 4B 位图文件大小，单位为B
	unsigned short	bfReserved1;			// 2B 保留
	unsigned short	bfReserved2;			// 2B 保留
	unsigned long	bfOffBits;			// 4B 文件头开始到图像实际数据之间的偏移量，单位为B
}BmpFileHeader;

typedef struct
{
	unsigned long	biSize;				// 4B 数据结构所需要的字节大小
	long			biWidth;		// 4B 图像宽度
	long			biHeight;		// 4B 图像高度
	unsigned short	biPlane;			// 2B 颜色平面数
	unsigned short	biBitCount;			// 2B 图像位深度：1，4，8，16，24，32
	unsigned long	biCompression;			// 4B 图像数据压缩类型：0(BI_RGB,不压缩),1(BI_RLE8，8比特游程编码),2(BI_RLE4，4比特游程编码),3(BI_BITFIELDS，比特域),4(BI_JPEG),5(BI_PNG)
	unsigned long	biSizeImage;			// 4B 图像大小，用BI_RGB格式时，可设置为0.
	long			biXPelsPerMeter;	// 4B 水平分辨率
	long			biYPelsPerMeter;	// 4B 垂直分辨率
	unsigned long	biClrUsed;			// 4B 颜色索引数
	unsigned long	biClrImport;			// 4B 对图像显示有重要影响的颜色索引数，0表示都重要
}BmpInfoHeader;


typedef enum {
	BitCount_1 = 1,
	BitCount_4 = 4,
	BitCount_8 = 8,
	BitCount_16 = 16,
	BitCount_24 = 24,
	BitCount_32 = 32
}BmpBitCount;

typedef struct
{
	unsigned char rgbBlue;  //该颜色的蓝色分量  
	unsigned char rgbGreen; //该颜色的绿色分量  
	unsigned char rgbRed;   //该颜色的红色分量
	unsigned char rgbReserved; //保留值  
} RgbQuad;

typedef struct
{
	int width;
	int height;
	int channels;
	unsigned char* imageData;
}Image;


#define BMP (0x4d42)

typedef struct {
	u8 val[4];				// 0:B 1:G 2:R 3:alpha
}PixelData;

typedef struct {
	u8 val[4];				// b g r alpha
}PaletteData;

class BmpPicture// : virtual public PictureBase
{
public:

	BmpPicture(string filePath) {
		bmpPath = filePath;
	}
	//BmpPicture();
	//~BmpPicture();

public:

	bool GetPalette() {
		ifs.seekg(sizeof(BmpFileHeader)+sizeof(BmpInfoHeader), std::ios::beg);
		for (int i = 0; i < infoHeader.biClrUsed; i++) {
			ifs.read(reinterpret_cast<char*>(&palette_data[i]), sizeof(u32));
		}
		return true;
	}

	bool GetBmpHeader(BmpFileHeader& fileHr, BmpInfoHeader& infoHr) {
		if (bmpPath.empty())
			return false;

		ifs.open(bmpPath, std::ios::in | std::ios::binary);
		if (!ifs.is_open())
			return false;

		ifs.seekg(0, std::ios::beg);
		ifs.read(reinterpret_cast<char*>(&bfType), sizeof(u16));
		if (IsBmp()) {
			// read bmp file header
			ifs.seekg(sizeof(u16), std::ios::beg);
			ifs.read(reinterpret_cast<char*>(&fileHr), sizeof(BmpFileHeader));
			// read bmp info header
			ifs.seekg(sizeof(u16)+sizeof(BmpFileHeader), std::ios::beg);
			ifs.read(reinterpret_cast<char*>(&infoHr), sizeof(BmpInfoHeader));

			imWidth = infoHr.biWidth;
			imHeight = infoHr.biHeight;
		}
		return true;
	}

	bool ImReadBitCount_24_32() {
		int offset_size = 0;
		offset_size = sizeof(BmpFileHeader) + sizeof(BmpInfoHeader) + infoHeader.biClrUsed * 4 + 2;
		ifs.seekg(offset_size, std::ios::beg);
		pixel_data.resize(imHeight * imWidth);
		int iLineByteCnt = (((imWidth * infoHeader.biBitCount) + 31) >> 5) << 2;
		u32 m_iImageDataSize = iLineByteCnt * imHeight;
		u8 skip = 4 - ((imWidth * infoHeader.biBitCount) >> 3) & 3;
		
		u32 pixelData = 0;
		for (int i = 0; i < imHeight; i++) {
			for (int j = 0; j < imWidth; j++) {
				if (infoHeader.biBitCount == BitCount_24) {
					ifs.read(reinterpret_cast<char*>(&pixelData), sizeof(u8) * 3);
					pixel_data[i*imWidth + j].val[2] = (pixelData & 0xff0000) >> 16;	// R
					pixel_data[i*imWidth + j].val[1] = (pixelData & 0xff00) >> 8;		// G
					pixel_data[i*imWidth + j].val[0] = pixelData & 0xff;				// B
				}
				else if (BitCount_32 == infoHeader.biBitCount) {
					ifs.read(reinterpret_cast<char*>(&pixelData), sizeof(u8) * 4);
					pixel_data[i*imWidth + j].val[3] = (pixelData & 0xff000000) >> 24;	// alpha
					pixel_data[i*imWidth + j].val[2] = (pixelData & 0xff0000) >> 16;	// R
					pixel_data[i*imWidth + j].val[1] = (pixelData & 0xff00) >> 8;		// G
					pixel_data[i*imWidth + j].val[0] = pixelData & 0xff;				// B
				}
			}
			ifs.seekg(skip, std::ios::cur);
		}

		return true;
	}

	bool ImRead() {
		if (GetBmpHeader(fileHeader, infoHeader)) {
			GetPalette();
			switch (infoHeader.biBitCount) {
			case BitCount_1:
				break;
			case BitCount_4:
				break;
			case BitCount_8:
				break;
			case BitCount_16:
				break;
			case BitCount_24:
			case BitCount_32:
				ImReadBitCount_24_32();
				break;
			default:
				break;
			}
			ifs.close();
		}
		else{
			return false;
		}
		return true;
	}

	bool SaveBmp(string saveFilePath, u32 hScaler = 1, u32 wScaler = 1) {
		if (saveFilePath.empty())
			return false;

		ifs.open(saveFilePath, std::ios::out | std::ios::binary);
		if (!ifs.is_open())
			return false;

		imHeight = infoHeader.biHeight *= hScaler;
		imWidth = infoHeader.biWidth *= wScaler;
		fileHeader.bfSize = 54 + ((imHeight * imWidth)*(infoHeader.biBitCount)>>3);

		ifs.write(reinterpret_cast<char*>(&bfType), sizeof(bfType));
		ifs.write(reinterpret_cast<char*>(&fileHeader), sizeof(fileHeader));
		ifs.write(reinterpret_cast<char*>(&infoHeader), sizeof(infoHeader));

		int iLineByteCnt = (((imWidth * infoHeader.biBitCount) + 31) >> 5) << 2;
		u32 m_iImageDataSize = iLineByteCnt * imHeight;
		u8 skip = 4 - ((imWidth * infoHeader.biBitCount) >> 3) & 3;

		u32 invalid_data = 0;
		for (int h = 0; h < imHeight; h++) {
			for (int w = 0; w < imWidth; w++) {
				//ifs.write(reinterpret_cast<char*>(&pixel_data[h*imWidth + w].val[0]), sizeof(u8)); // B
				//ifs.write(reinterpret_cast<char*>(&pixel_data[h*imWidth + w].val[1]), sizeof(u8)); // G
				//ifs.write(reinterpret_cast<char*>(&pixel_data[h*imWidth + w].val[2]), sizeof(u8)); // R

				ifs.write(reinterpret_cast<char*>(&pixel_data[(h % imHeight)*imWidth + (w % imWidth)].val[0]), sizeof(u8)); // B
				ifs.write(reinterpret_cast<char*>(&pixel_data[(h % imHeight)*imWidth + (w % imWidth)].val[1]), sizeof(u8)); // G
				ifs.write(reinterpret_cast<char*>(&pixel_data[(h % imHeight)*imWidth + (w % imWidth)].val[2]), sizeof(u8)); // R
			}
			ifs.write(reinterpret_cast<char*>(&invalid_data), skip); // R
		}

		ifs.close();
		return true;
	}

	int GetImWidth() {
		return imWidth;
	}

	int GetImHeight() {
		return imHeight;
	}

	ImageFileType GetImType() {
		return imType;
	}

	//显示位图文件头信息   
	void showBmpHead() {
		cout << "位图文件头:" << endl;
		//cout<<"bfType value is "<<hex<<fileHeader.bfType<<endl;   
		cout << "文件大小:" << fileHeader.bfSize << endl;
		//printf("文件大小:%dn",fileHeader.bfSize);   
		cout << "保留字_1:" << fileHeader.bfReserved1 << endl;
		cout << "保留字_2:" << fileHeader.bfReserved2 << endl;
		cout << "实际位图数据的偏移字节数:" << fileHeader.bfOffBits << endl << endl;
	}

	void showBmpInforHead() {
		cout << "位图信息头:" << endl;
		cout << "结构体的长度:" << infoHeader.biSize << endl;
		cout << "位图宽:" << infoHeader.biWidth << endl;
		cout << "位图高:" << infoHeader.biHeight << endl;
		cout << "biPlanes平面数:" << infoHeader.biPlane << endl;
		cout << "biBitCount采用颜色位数:" << infoHeader.biBitCount << endl;
		cout << "压缩方式:" << infoHeader.biCompression << endl;
		cout << "biSizeImage实际位图数据占用的字节数:" << infoHeader.biSizeImage << endl;
		cout << "X方向分辨率:" << infoHeader.biXPelsPerMeter << endl;
		cout << "Y方向分辨率:" << infoHeader.biYPelsPerMeter << endl;
		cout << "使用的颜色数:" << infoHeader.biClrUsed << endl;
		cout << "重要颜色数:" << infoHeader.biClrImport << endl;
	}

private:
	bool IsBmp() {
		if (BMP == bfType) {
			return true;
		}
		return false;
	}

private:
	BmpFileHeader fileHeader;
	BmpInfoHeader infoHeader;
	int imWidth;
	int imHeight;
	ImageFileType imType;
	u16 bfType;
	std::fstream ifs;
	string bmpPath;
public:
	vector<PixelData> pixel_data;
	vector<PaletteData> palette_data;
};

