#pragma once
#include "type_base.h"
#include <vector>

using namespace std;

typedef enum {
	// 2x2		//G R
	GRBG = 0,	//B G
}Bayer;

typedef struct {
	u8 val[3];				// 0:B 1:G 2:R
}RgbData;


class IspRaw {
public:
	IspRaw() {
	}

	~IspRaw() {
	}

	int SetImageHeight(u32 &height) {
		imHeight = height;
		return 0;
	}

	int SetImageWidth(u32 &width) {
		imWidth = width;
		return 0;
	}

	int GetRawData(vector<u8> &data, const Bayer &bayer) {
		bayerMatrix = bayer;
		int size = data.size();
		for (auto it : data) {
			rawData.push_back(it);
		}

		return size;
	}

	int Demosaic() {
		switch (bayerMatrix) {
		case GRBG:
			DemosaicGrbg();
			break;
		default:
			break;
		}

		return 0;
	}

private:
	int DemosaicGrbg() {
		int heigtFlag = imHeight % 2;
		int widthFlag = imWidth % 2;
		rgbData.resize(imHeight*imWidth);
		for (int h = 0; h < imHeight; h++) {
			for (int w = 0; w < imWidth; w++) {
				u8 R = 0;
				u8 G = 0;
				u8 B = 0;
				if (h % 2 == 0) {
					if (w % 2 == 0) {
						u32 g_upB = (h - 1)*imWidth + w;
						u32 g_downB = (h + 1)*imWidth + w;
						u32 g_leftR = h*imWidth + w - 1;
						u32 g_rightR = h*imWidth + w + 1;

						if (w == 0) {
							g_leftR = g_rightR;
						}
						if (h == 0) {
							g_upB = g_downB;
						}

						if (heigtFlag == 1) {
							if (h == imHeight - 1) {
								g_downB = g_upB;
							}
						}

						if (widthFlag == 1) {
							if (w == imWidth-1) {
								g_rightR = g_leftR;
							}

						}

						//G
						G = rawData[h*imWidth + w];
						R = (rawData[g_leftR] + rawData[g_rightR]) / 2;;
						B = (rawData[g_upB] + rawData[g_downB]) / 2;
					}
					else {
						u32 r_upG = (h - 1)*imWidth + w;
						u32 r_downG = (h + 1)*imWidth + w;
						u32 r_leftG = h*imWidth + w - 1;
						u32 r_rightG = h*imWidth + w + 1;
						u32 r_leftupB = (h - 1)*imWidth + w - 1;
						u32 r_leftdownB = (h + 1)*imWidth + w - 1;
						u32 r_rightupB = (h - 1)*imWidth + w + 1;
						u32 r_rightdownB = (h + 1)*imWidth + w + 1;

						if (w == imWidth - 1) {
							if (widthFlag == 0) {
								r_rightG = r_leftG;
								r_rightupB = r_leftupB;
								r_rightdownB = r_leftdownB;
							}
						}
						if (h == 0) {
							r_upG = r_downG;
							r_leftupB = r_leftdownB;
							r_rightupB = r_rightdownB;
							if (w == imWidth - 1) {
								if (widthFlag == 0) {
									r_rightG = r_leftG;
									r_rightupB = r_leftdownB;
									r_rightdownB = r_leftdownB;
								}
							}
						}
						if (h == imHeight - 1) {
							if (heigtFlag == 1) {
								r_downG = r_upG;
								r_leftdownB = r_leftupB;
								r_rightdownB = r_rightupB;
								if (w == imWidth - 1) {
									if (widthFlag == 0) {
										r_leftdownB = r_leftupB;
										r_rightupB = r_leftdownB;
										r_rightdownB = r_leftdownB;
									}
								}
							}
						}

						//R
						R = rawData[h*imWidth + w];
						G = (rawData[r_upG] + rawData[r_downG] + rawData[r_leftG] + rawData[r_rightG])/4;
						B = (rawData[r_leftupB] + rawData[r_leftdownB] + rawData[r_rightupB] + rawData[r_rightdownB])/4;
					}
				}
				else {
					if (w%2 == 0) {
						u32 b_upG = (h-1)*imWidth + w;
						u32 b_downG = (h+1)*imWidth + w;
						u32 b_leftG = h*imWidth + w-1;
						u32 b_rightG = h*imWidth + w+1;
						u32 b_leftupR = (h-1)*imWidth + w-1;
						u32 b_leftdownR = (h+1)*imWidth + w-1;
						u32 b_rightupR = (h-1)*imWidth + w+1;
						u32 b_rightdownR = (h+1)*imWidth + w+1;

						if (w == 0) {
							b_leftG = b_rightG;
							b_leftupR = b_rightupR;
							b_leftdownR = b_rightdownR;
						}
						if (w == imWidth - 1) {
							if (widthFlag == 1) {
								b_rightG = b_leftG;
								b_rightupR = b_leftupR;
								b_rightdownR = b_leftdownR;
							}
						}
						if (h == imHeight - 1) {
							if (heigtFlag == 0) {
								b_downG = b_upG;
								b_leftdownR = b_leftupR;
								b_rightdownR = b_rightupR;
							}
						}

						//B
						B = rawData[h*imWidth + w];
						G = (rawData[b_upG] + rawData[b_downG] + rawData[b_leftG] + rawData[b_rightG])/4;
						R = (rawData[b_leftupR] + rawData[b_leftdownR] + rawData[b_rightupR] + rawData[b_rightdownR])/4;
					}
					else {
						u32 g_upR = (h-1)*imWidth + w;
						u32 g_downR = (h+1)*imWidth + w;
						u32 g_leftB = h*imWidth + w-1;
						u32 g_rightB = h*imWidth + w+1;

						if (w == imWidth - 1) {
							if (widthFlag == 0) {
								g_rightB = g_leftB;
							}
						}
						if (h == imHeight - 1) {
							if (heigtFlag == 0) {
								g_downR = g_upR;
							}
						}

						//G
						G = rawData[h*imWidth + w];
						B = (rawData[g_leftB] + rawData[g_rightB])/2;
						R = (rawData[g_upR] + rawData[g_downR])/2;
					}
				}
				//B
				rgbData[h*imWidth + w].val[0] = B;
				//G
				rgbData[h*imWidth + w].val[1] = G;
				//R
				rgbData[h*imWidth + w].val[2] = R;
			}
		}
		return 0;
	}

public:
		vector<RgbData> rgbData;	//0:B 1:G 2:R

private:
	u32 imHeight;
	u32 imWidth;
	vector<int> rawData;
	Bayer bayerMatrix;
};


