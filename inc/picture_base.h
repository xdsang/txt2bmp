#pragma once

#include <vector>

using namespace std;

enum ImageFileType
{
	FILE_TYPE_BMP = 0x0000,
	FILE_TYPE_PNG,
	FILE_TYPE_JPG,
	FILE_TYPE_JPEG,
	FILE_TYPE_TIF
};

enum ImageFormat
{
	IMAGE_BINARY = 0x0001,	// ��ֵͼ��
	IMAGE_GREY = 0x0008,	// �Ҷ�ͼ��
	IMAGE_RGB = 0x0018,	// ��ɫRGBͼ��
	IMAGE_BGR = 0x0019,	// ��ɫBGRͼ��
	IMAGE_RGBA = 0x0020,	// ��ɫRGBAͼ��
	IMAGE_BGRA = 0x0021	// ��ɫBGRAͼ��
};


class PictureBase {
public:
	PictureBase();
	PictureBase(int width, int height, ImageFileType imType);
	~PictureBase();

	//virtual void imRead() = 0;
	//virtual int getImWidth() = 0;
	//virtual int getImHeight() = 0;
	//virtual ImageFileType getImType() = 0;

private:
	int width;
	int height;
	ImageFileType imType;
};