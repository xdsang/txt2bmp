#pragma once
#include <iostream>

typedef unsigned short		u16;
typedef unsigned int		u32;
typedef int					s32;
typedef unsigned char		u8;
typedef char				s8;
typedef unsigned int		uint32;
typedef int					int32;
typedef unsigned char		uint8;
typedef char				int8;
typedef unsigned short		uint16;
typedef short				int16;
typedef unsigned long long  u64;
typedef long long           s64;


