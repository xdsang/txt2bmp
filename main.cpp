#include "picture_bmp.h"
#include "isp_raw.h"

int main(int argc, char **argv) {
	BmpPicture bm(BMP_FILE_PATH);
	bm.ImRead();
	bm.showBmpHead();
	bm.showBmpInforHead();

	// get raw data GRBG
	u32 height = bm.GetImHeight();
	u32 width = bm.GetImWidth();
	vector<u8> data;
	for (int h = 0; h < height; h++) {
		for (int w = 0; w < width; w++) {
			u8 val = 0;
			if (h % 2 == 0) {
				if (w % 2 == 0) {
					val = bm.pixel_data[h*width + w].val[1];
				}
				else {
					val = bm.pixel_data[h*width + w].val[2];
				}
			}
			else {
				if (w % 2 == 0) {
					val = bm.pixel_data[h*width + w].val[0];
				}
				else {
					val = bm.pixel_data[h*width + w].val[1];
				}
			}
			data.push_back(val);
		}
	}
	
	IspRaw ir;
	ir.SetImageHeight(height);
	ir.SetImageWidth(width);
	ir.GetRawData(data, GRBG);

	ir.Demosaic();

	for (int i = 0; i < ir.rgbData.size();i++) {
		bm.pixel_data[i].val[0] = ir.rgbData[i].val[0];
		bm.pixel_data[i].val[1] = ir.rgbData[i].val[1];
		bm.pixel_data[i].val[2] = ir.rgbData[i].val[2];
	}

	string savePath = "D:\\ForMe\\TestCode\\C\\txt2bmp\\out.bmp";

	bm.SaveBmp(savePath, 1, 1);

	return 0;
}
